var friendsList = [];

var fbLogin = function () {
    if (!window.cordova) {
        var appId = prompt("Enter FB Application ID", "");
        facebookConnectPlugin.browserInit(appId);
    }
    facebookConnectPlugin.login(["public_profile", "user_friends"], fbLoginSuccess, fbLoginError);
}

var fbLoginError = function (error) {
    console.log("Facebook login error: " + error);
}

var fbLoginSuccess = function (userData) {
    console.log("Facebook login success!");
    var myFbId = userData.authResponse.userID;
    var myFbToken = userData.authResponse.accessToken;
    localStorage.setItem('fbId', myFbId);
    localStorage.setItem('fbToken', myFbToken);
    
    facebookConnectPlugin.api('/me', null, function(response) {
        var myFbName = response.name;
        localStorage.setItem('fbName', myFbName);
        var JSONdata = {
            studentFbId:myFbId,
            studentName: myFbName,
            studentFbToken: myFbToken,
            studentStatus: 1
        };
        registerStudentOnServer(JSONdata);
    });
}

function populateFriendList(callback){
    getAcceptedInvitation(function (data){
        friendsList = [];
        for (var invitation in data) {
            if (data.hasOwnProperty(invitation)) {
                var i = data[invitation];
                if (i.invitationSender.studentFbId != localStorage.getItem('fbId')){
                    friendsList.push(i.invitationSender);
                } else {
                    friendsList.push(i.invitationRecipient);
                }
            }
        }
        callback();
    });
}

function templateFriendsList(fID, fName, fStatus){
    var template =
    '<li class="contact-item">' +
        '<a href="" class="item-link" onclick="sendInvitation(this);" data-friendid="'+ fID + '">' +
            '<div class="item-content">' +
                '<div class="item-media">' +
                    '<img src="https://graph.facebook.com/' + fID + '/picture?type=large">' +
                '</div>' +
                '<div class="item-inner">' +
                    '<div class="item-title-row">' +
                        '<div class="item-title">' + fName + '</div>' +
                    '</div>' +
                    '<div class="item-subtitle">'+ fStatus + '</div>' +
                '</div>' +
            '</div>' +
        '</a>' +
    '</li>';
    return template;
}

function templatePendingList(fID, fName){
    var template =
    '<li class="swipeout">' +
        '<div class="swipeout-content item-content">' +
            '<div class="item-media">' +
                '<img src="https://graph.facebook.com/' +  fID + '/picture?type=large">' +
            '</div>' +
            '<div class="item-inner">' + fName + '</div>' +
        '</div>' +
        '<div class="swipeout-actions-right">' +
            '<a href="#" class="swipeout-delete bg-green accept-invite" data-friendid="'+ fID  +'">Aceitar</a>' +
            '<a href="#" class="swipeout-delete swipeout-overswipe reject-invite" data-friendid="'+ fID +'">Rejeitar</a>' +
        '</div>' +
    '</li>';
    return template;
}

var printAcceptedFriends = function () {
    if (typeof friendsList !== 'undefined' && friendsList.length > 0) {
        var results = '';
        facebookConnectPlugin.api( "me/friends?fields=picture,name", ["public_profile", "user_friends"], function (response) {
            var friendData = response.data.sort(sortMethod);
            for (var i = 0; i < friendData.length; i++) {
                for (var a = 0; a < friendsList.length; a++){
                    if (friendData[i].id == friendsList[a].studentFbId){
                        results += templateFriendsList(friendData[i].id, friendData[i].name, convertStatus(friendsList[a].studentStatus));
                    }
                }
            }
            $('#friends-list').empty();
            $('#friends-list-empety').css({ display: "none" });
            $('#friends-list').append(results);
            updateMyFriendsPosition();
        });
    } else {
        $('#friends-list').empty();
        $('#friends-list-empety').css({ display: "block" });
    }
}

var getAcceptedFriends = function () {
        populateFriendList(printAcceptedFriends);
}

var getFriendsList = function (callback) {
    facebookConnectPlugin.api( "me/friends?fields=picture,name", ["public_profile", "user_friends"],
        function (response) {
            var friendData = response.data.sort(sortMethod);
            var results = '<div class="list-block media-list contacts-list list-block-search searchbar-found">' +
                              '<ul id="friends-list" class="friends-list">';
            for (var i = 0; i < friendData.length; i++) {
                results += templateFriendsList(friendData[i].id, friendData[i].name, '');
            }
            results +=      '</ul>' +
                        '</div>';
            callback(results);
        }
    );
}

var getPendingFriends = function () {
    getPendingReceivedInvitation(function (data){
        var listOfIds = [];
        for (var invitation in data) {
            if (data.hasOwnProperty(invitation)) {
                var obj = data[invitation];
                listOfIds.push(obj.invitationSender.studentFbId);
            }
        }
        if (typeof listOfIds !== 'undefined' && listOfIds.length > 0) {
            var results = '';
            facebookConnectPlugin.api( "me/friends?fields=picture,name", ["public_profile", "user_friends"],
                function (response) {
                    var friendData = response.data.sort(sortMethod);
                    for (var i = 0; i < friendData.length; i++) {
                        for (var a = 0; a < listOfIds.length; a++){
                            if (friendData[i].id == listOfIds[a]){
                                results += templatePendingList(friendData[i].id, friendData[i].name );
                            }
                        }
                    }
                    $('#pending-list').append(results);
                }
            );
        }
    });
}

function sortMethod(a, b) {
    var x = a.name.toLowerCase();
    var y = b.name.toLowerCase();
    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

function convertStatus(statusNumber){
    var statusText = '';
    switch(statusNumber)
    {
        case 0: statusText = 'Indisponível';
            break;
        case 1: statusText = 'Conectado';
            break;
    }
    return statusText;
}

function getEventsList(callback){
    getEventsListService(function (eventList){
        var results = '<ul id="events-list" class="events-list">';
        for (var i = 0; i < eventList.length; i++) {
                results += '<li>' + eventList[i].content + '</li>';
        }
        results += '</ul>';
        callback(results);
    });
}

