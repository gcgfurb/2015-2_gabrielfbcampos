var server = 'http://192.168.1.5:8080/interacao/services';

function registerStudentOnServer(studentData){
    $.ajax({
        type : 'POST',
        url : server + '/student/login',
        crossDomain : true,
        data : JSON.stringify(studentData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
           pushNotification.register();
           window.location.href = "interacao.html";
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
           console.log("Error status: " + textStatus);
           console.log("Error type: " + errorThrown);
        }
    });
}

function updateMyPosition(studentData){
    $.ajax({
        type : 'POST',
        url : server + '/student/coordinates',
        crossDomain : true,
        data : JSON.stringify(studentData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
           console.log("updateMyPosition success");
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
           console.log("Error status: " + textStatus);
           console.log("Error type: " + errorThrown);
        }
    });
}

function updateInvitation(invitationData){
    $.ajax({
        type : 'POST',
        url : server + '/student/invite/update',
        crossDomain : true,
        data : JSON.stringify(invitationData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
           alert("Convite atualizado com sucesso!");
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
           alert("Não foi possível atualizar o convite!");
           console.log("Error status: " + textStatus);
           console.log("Error type: " + errorThrown);
        }
    });
}

function sendInvitation(element){
    var fbFriendId = element.getAttribute("data-friendid");
    var fbSenderId = localStorage.getItem('fbId');
    var invitationData = {
        invitationStatus: 0,
        invitationRecipient: {
            studentFbId: fbFriendId
        },
        invitationSender: {
            studentFbId: fbSenderId
        },
        actionStudent: {
            studentFbId: fbSenderId
        }
    };
    $.ajax({
        type : 'POST',
        url : server + '/student/invite',
        crossDomain : true,
        data : JSON.stringify(invitationData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
           alert("Convite enviado com sucesso!");
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
           alert("Não foi possível enviar o convite!");
           console.log("Error status: " + textStatus);
           console.log("Error type: " + errorThrown);
        }
    });
}

function getAcceptedInvitation(callback){
    var fbSenderId = localStorage.getItem('fbId');
    var invitationData = {
        invitationSender: {
            studentFbId: fbSenderId
        }
    };
    $.ajax({
        type : 'POST',
        url : server + '/student/invite/accepted',
        crossDomain : true,
        data : JSON.stringify(invitationData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
            callback(data);
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
            alert("Não foi possível buscar seus amigos!");
            console.log("Error status: " + textStatus);
            console.log("Error type: " + errorThrown);
        }
    });
}

function getPendingReceivedInvitation(callback){
    var fbSenderId = localStorage.getItem('fbId');
    var invitationData = {
        invitationRecipient: {
            studentFbId: fbSenderId
        }
    };
    $.ajax({
        type : 'POST',
        url : server + '/student/invite/pending',
        crossDomain : true,
        data : JSON.stringify(invitationData),
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
            callback(data);
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
            alert("Não foi possível buscar seus convites pendentes!");
            console.log("Error status :" + textStatus);
            console.log("Error type :" + errorThrown);
        }
    });
}

function getEventsListService(callback){
    $.ajax({
        type : 'GET',
        url : server + '/student/listevents',
        crossDomain : true,
        dataType : 'json',
        contentType: 'application/json; charset=utf-8',
        success : function(data){
            callback(data);
        },
        error : function(XMLHttpRequest,textStatus, errorThrown) {
            alert("Não foi possível buscar a lista de eventos!");
            console.log("Error status :" + textStatus);
            console.log("Error type :" + errorThrown);
        }
    });
}

/*function getPendingSentInvitation(callback){
    var fbSenderId = localStorage.getItem('fbId');
    var invitationData = {
                            invitationSender: {
                                studentFbId: fbSenderId
                            }
                         };
    $.ajax({
           type : 'POST',
           url : server + '/student/invite/sent',
           crossDomain : true,
           data : JSON.stringify(invitationData),
           dataType : 'json',
           contentType: 'application/json; charset=utf-8',
           success : function(data){
                callback(data);
           },
           error : function(XMLHttpRequest,textStatus, errorThrown) {
                alert("Não foi possível buscar seus convites enviados.");
                console.log("Error status :" + textStatus);
                console.log("Error type :" + errorThrown);
           }
           });
}

function getStudent(fbId){
    var studentData = { studentFbId: fbId };
    $.ajax({
           type : 'POST',
           url : server + '/student/invite/accepted',
           crossDomain : true,
           data : JSON.stringify(studentData),
           dataType : 'json',
           contentType: 'application/json; charset=utf-8',
           success : function(data){
                console.log("getStudent success");
           },
           error : function(XMLHttpRequest,textStatus, errorThrown) {
                alert("Não foi possível buscar seus amigos!");
                console.log("Error status :" + textStatus);
                console.log("Error type :" + errorThrown);
           }
           });
}*/