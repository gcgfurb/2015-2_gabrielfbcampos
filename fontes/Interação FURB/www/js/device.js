var app = {
    initialize: function() {
        this.bindEvents();
    },
    
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('offline', this.deviceOnline, false);
        document.addEventListener('online', this.deviceOffline, false);
    },

    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },

    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    },
    
    deviceOnline: function(id) {
        console.log('Dispositivo Online');
    },
    
    deviceOffline: function(id) {
        console.log('Dispositivo Offline');
    }
};
