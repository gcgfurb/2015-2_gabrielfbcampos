var pushNotification = {

    initialize: function () {
        document.addEventListener('deviceready', this.register, false);
    },
    
    register: function () {
        var myFbIdA = localStorage.getItem('fbId');
        var pushConfig = {
            pushServerURL: "http://192.168.1.5:8080/ag-push/",
            alias: myFbIdA,
            ios: {
                variantID: "a3dc497a-8e7c-4a9f-98d2-8b481f73802b",
                variantSecret: "7f1df0a4-06bb-427d-9188-763ec05067b1"
            },
            android: {
                senderID: "37471308173",
                variantID: "849c1cb8-c81b-4015-a514-c44bf23a3149",
                variantSecret: "ea5b7fbf-3831-4f0a-8ce6-7a2f8b173211"
            }
        };
        
        if (typeof push !== 'undefined') {
            push.register(pushNotification.onNotification, successHandler, errorHandler, pushConfig);
        } else {
            console.log('Push plugin não instalado!');
        }
    
        function successHandler() {
            console.log('Push registrado com sucesso!');
        }
    
        function errorHandler(error) {
            console.log('Push erro:' + error);
        }
    },
    
    onNotification: function (event) {
        console.log('Novo evento push: ' + event);        
    }
};
