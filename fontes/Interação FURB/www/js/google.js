var map;
var watchID;
var myLatitude;
var myLongitude;
var geolocationOptions = { frequency: 3000 };
var interval;
var markers = [];

document.addEventListener("deviceready", function() {
    map = plugin.google.maps.Map.getMap(document.getElementById("mapCanvas"));
    map.setOptions({
        'controls':{
            'compass': true,
            'myLocationButton': true,
            'indoorPicker': true
        },
        'gestures': {
            'scroll': true,
            'tilt': true,
            'rotate': true,
            'zoom': true
        }
    });
    map.addEventListener(plugin.google.maps.event.MAP_READY, mapSuccess);
}, false);

var mapSuccess = function(position) {
    watchID = navigator.geolocation.watchPosition(geolocationSuccess, geolocationError, geolocationOptions);
};

var geolocationSuccess = function(position) {
    myLatitude = position.coords.latitude;
    myLongitude = position.coords.longitude;
    var myCoordinates = new plugin.google.maps.LatLng(myLatitude, myLongitude);
    var studentData = {
        studentFbId:localStorage.getItem('fbId'),
        studentLatitude: myLatitude,
        studentLongitude: myLongitude,
        studentStatus: 1
    };
    updateMyPosition(studentData);
};

function geolocationError(error) {
    switch(error.code)
    {
        case error.PERMISSION_DENIED: alert("Usuário não compartilhou sua geolocalização");
            break;
        case error.POSITION_UNAVAILABLE: alert("Não pode encontrar a localização atual");
            break;
        case error.TIMEOUT: alert("Excedeu tempo limete de busca");
            break;
        default: alert("Erro desconhecido");
            break;
    }
}

function clearMarkers(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

function updateMyFriendsPosition(){
    if (typeof friendsList !== 'undefined' && friendsList.length > 0){
        async.map(friendsList, function(friendData, callback) {
            map.addMarker({ visible: false }, function(marker) {
                map.on("friend_change_" + friendData.studentFbId, function(friend) {
                    var position = new plugin.google.maps.LatLng(friend.studentLatitude, friend.studentLongitude);
                    marker.setPosition(position);
                    marker.setVisible(true);
                    marker.set("photo_url", "https://graph.facebook.com/" + friend.studentFbId + "/picture");
                });
                marker.on(plugin.google.maps.event.MARKER_CLICK, function() { });
                markers.push(marker);
                callback(null, marker);
            });
        },function(markers) {
            interval = setInterval(function() {
                getAcceptedInvitation(function (d){
                    friendsList = [];
                    for (var i in d) {
                        if (d.hasOwnProperty(i)) {
                            var o = d[i];
                            if (o.invitationSender.studentFbId != localStorage.getItem('fbId')){
                                friendsList.push(o.invitationSender);
                            } else {
                                friendsList.push(o.invitationRecipient);
                            }
                        }
                    }
                    if (!friendsList) {
                        return;
                    }
                    friendsList.forEach(function(f, i) {
                        map.trigger("friend_change_" + f.studentFbId, f);
                    });
                });
            },10000);
        });
    }
}
