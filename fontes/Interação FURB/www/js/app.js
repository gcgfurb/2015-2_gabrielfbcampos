var myApp = new Framework7({ init: false });
var $$ = Dom7;
var ptrContent = $$('.pull-to-refresh-content');

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true
});

myApp.onPageInit('share', function (page) {
    getFriendsList(function (friendsList) {
        $$(page.container).find('.page-content').append(friendsList);
    });
});

myApp.onPageInit('events', function (page) {
    getEventsList(function (eventsList) {
        $$(page.container).find('.content-block').append(eventsList);
    });
});

ptrContent.on('refresh', function (e) {
    clearInterval(interval);
    clearMarkers(null);
    getAcceptedFriends();
    myApp.pullToRefreshDone();
});

$$('.panel-left').on('open', function () {
    if ($$('body').hasClass('with-panel-left-reveal')) {
        console.log('Left Panel is opened')
    }else{
        $('#pending-list').empty();
        getPendingFriends();
    }
});

$$(document).on('click', '.reject-invite', function (e){
    var fbSenderId = $$(this).data("friendid");
    var fbRecipientId = localStorage.getItem('fbId');
    var invitationData = {
        invitationStatus: 2,
        invitationRecipient: {
            studentFbId: fbRecipientId
        },
        invitationSender: {
            studentFbId: fbSenderId
        },
        actionStudent: {
            studentFbId: fbRecipientId
        }
    };
    updateInvitation(invitationData);
});

$$(document).on('click', '.accept-invite', function (e){
    var fbSenderId = $$(this).data("friendid");
    var fbRecipientId = localStorage.getItem('fbId');
    var invitationData = {
        invitationStatus: 1,
        invitationRecipient: {
            studentFbId: fbRecipientId
        },
        invitationSender: {
            studentFbId: fbSenderId
        },
        actionStudent: {
            studentFbId: fbRecipientId
        }
    };
    updateInvitation(invitationData);
});

myApp.init();
//myApp.onPageInit('interacao', function (page) { });