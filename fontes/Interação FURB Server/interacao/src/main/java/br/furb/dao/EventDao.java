package br.furb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import br.furb.model.Event;

public class EventDao {
	
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("interacao");
		this.entityManager = emf.createEntityManager();
		return this.entityManager;
	}
	
	public void createEvent(Event event) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(event);
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}
	
	public List<Event> getAllEvents() {
		Query query = this.getEntityManager().createQuery("from Event");
		return query.getResultList();
	}
}
