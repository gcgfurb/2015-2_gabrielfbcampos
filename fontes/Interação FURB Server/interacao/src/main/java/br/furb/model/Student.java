package br.furb.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Student implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Long studentFbId;
	
	private String studentName;
	
	private String studentLatitude;
	
	private String studentLongitude;
	
	private String studentFbToken;
	
	//Offline: 0  Online: 1
	private int studentStatus;
	
	@Temporal(javax.persistence.TemporalType.TIMESTAMP)
	private Date studentLastAccess;
	
	@Transient
	private List<Invitation> invitationSentList;
	
	@Transient
	private List<Invitation> invitationReceivedList;

	public Long getStudentFbId() {
		return studentFbId;
	}

	public void setStudentFbId(Long studentFbId) {
		this.studentFbId = studentFbId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentLatitude() {
		return studentLatitude;
	}

	public void setStudentLatitude(String studentLatitude) {
		this.studentLatitude = studentLatitude;
	}

	public String getStudentLongitude() {
		return studentLongitude;
	}

	public void setStudentLongitude(String studentLongitude) {
		this.studentLongitude = studentLongitude;
	}

	public String getStudentFbToken() {
		return studentFbToken;
	}

	public void setStudentFbToken(String studentFbToken) {
		this.studentFbToken = studentFbToken;
	}
	
	public int getStudentStatus() {
		return studentStatus;
	}

	public void setStudentStatus(int studentStatus) {
		this.studentStatus = studentStatus;
	}

	public Date getStudentLastAccess() {
		return studentLastAccess;
	}

	public void setStudentLastAccess(Date studentLastAccess) {
		this.studentLastAccess = studentLastAccess;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Invitation> getInvitationSentList() {
		return invitationSentList;
	}

	public void setInvitationSentList(List<Invitation> invitationSentList) {
		this.invitationSentList = invitationSentList;
	}

	public List<Invitation> getInvitationReceivedList() {
		return invitationReceivedList;
	}

	public void setInvitationReceivedList(List<Invitation> invitationReceivedList) {
		this.invitationReceivedList = invitationReceivedList;
	}
}
