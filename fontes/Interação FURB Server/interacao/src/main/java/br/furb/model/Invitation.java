package br.furb.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Invitation implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne
	@JoinColumn(referencedColumnName = "studentFbId")
	private Student invitationRecipient;
	
	@Id
	@ManyToOne
	@JoinColumn(referencedColumnName = "studentFbId")
	private Student invitationSender;

	@ManyToOne
	@JoinColumn(referencedColumnName = "studentFbId")
	private Student actionStudent;
	
	//Pending: 0  Accepted: 1  Declined: 2  Blocked: 3
	private int invitationStatus;
	
	public Student getInvitationRecipient() {
		return invitationRecipient;
	}
	
	public void setInvitationRecipient(Student invitationRecipient) {
		this.invitationRecipient = invitationRecipient;
	}
	
	public Student getInvitationSender() {
		return invitationSender;
	}
	
	public void setInvitationSender(Student invitationSender) {
		this.invitationSender = invitationSender;
	}
	
	public int getInvitationStatus() {
		return invitationStatus;
	}
	
	public void setInvitationStatus(int invitationStatus) {
		this.invitationStatus = invitationStatus;
	}
	
	public Student getActionStudent() {
		return actionStudent;
	}
	
	public void setActionStudent(Student actionStudent) {
		this.actionStudent = actionStudent;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
