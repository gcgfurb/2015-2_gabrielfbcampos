package br.furb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import br.furb.model.Invitation;
import br.furb.model.Student;

public class InvitationDao {
	
	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("interacao");
		this.entityManager = emf.createEntityManager();
		return this.entityManager;
	}
	
	public List<Invitation> getAllInivitations() {
		Query query = this.getEntityManager().createQuery("from Invitation");
		return query.getResultList();
	}
		
	public List<Invitation> getAcceptedInvitation(Invitation invitation) {
		Query query = this.getEntityManager().createQuery("from Invitation where (invitationSender = :invitationSender" +
																			" or invitationRecipient = :invitationSender)" +
																			" and invitationStatus = :invitationStatus");
		query.setParameter("invitationSender", invitation.getInvitationSender());
		query.setParameter("invitationStatus", 1);
		return query.getResultList();
	}
	
	public List<Invitation> getPendingReceivedInvitation(Invitation invitation) {
		Query query = this.getEntityManager().createQuery("from Invitation where invitationRecipient = :invitationRecipient" +
																			" and invitationStatus = :invitationStatus");
		query.setParameter("invitationRecipient", invitation.getInvitationRecipient());
		query.setParameter("invitationStatus", 0);
		return query.getResultList();
	}
	
	public List<Invitation> getPendingSentInvitation(Invitation invitation) {
		Query query = this.getEntityManager().createQuery("from Invitation where invitationSender = :invitationSender" +
																			" and invitationStatus = :invitationStatus");
		query.setParameter("invitationSender", invitation.getInvitationSender());
		query.setParameter("invitationStatus", 0);
		return query.getResultList();
	}
	
	public List<Invitation> checkInvitationExisit(Invitation invitation) {
		Query query = this.getEntityManager().createQuery("from Invitation where (invitationSender = :invitationSender" +
																			" or invitationRecipient = :invitationSender)" +
																			" and (invitationSender = :invitationRecipient" +
																			" or invitationRecipient = :invitationRecipient)");
		query.setParameter("invitationSender", invitation.getInvitationSender());
		query.setParameter("invitationRecipient", invitation.getInvitationRecipient());
		return query.getResultList();
	}
	
	public void createInvitation(Invitation invitation) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			List<Invitation> list = checkInvitationExisit(invitation);
			if (!list.isEmpty()){
				Invitation i = list.get(0);
				if(i.getInvitationStatus() == 0 && i.getInvitationSender() != invitation.getInvitationSender()){
					i.setInvitationStatus(1);
					i.setActionStudent(invitation.getInvitationSender());
					mergeInvitation(i);
				}
			}else{
				tx.begin();
				em.persist(invitation);
			}
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}
	
	public void mergeInvitation(Invitation invitation) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.merge(invitation);
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}

	public void updateInvitation(Invitation invitation) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			Query query = em.createQuery("update Invitation set invitationStatus = :invitationStatus"
											+  " where invitationRecipient = :invitationRecipient"
											+  " and invitationSender = :invitationSender");
			query.setParameter("invitationStatus", invitation.getInvitationStatus());
			query.setParameter("invitationRecipient", invitation.getInvitationRecipient());
			query.setParameter("invitationSender", invitation.getInvitationSender());
			query.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}
}
