package br.furb.service;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import br.furb.dao.EventDao;
import br.furb.dao.InvitationDao;
import br.furb.dao.StudentDao;
import br.furb.model.Event;
import br.furb.model.Invitation;
import br.furb.model.Student;

@Path("/student")
public class InteracaoService {
	
    @GET
    @Path("/message/{param}")
    public String printMessage(@PathParam("param") String msg) {
        Event event = new Event();
        event.setContent(msg);
        new EventDao().createEvent(event);
        new NotificationSender().sendEventNotification(event);
    	String result = "Restful Return! : " + msg;
        return result;
    }
    
    @GET
    @Path("/listevents")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Event> getEventList() {
        return new EventDao().getAllEvents();
    }
	
	@POST
	@Path("/invite")
	@Consumes({MediaType.APPLICATION_JSON})
	public void invite(Invitation invitation) {
		new InvitationDao().createInvitation(invitation);
	    new NotificationSender().sendInvitationNotification(invitation);
	}
	
	@POST
	@Path("/invite/accepted")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public List<Invitation> getAcceptedInvitation(Invitation invitation) {
		return new InvitationDao().getAcceptedInvitation(invitation);
	}
	
	@POST
	@Path("/invite/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public void getUpdateInvitation(Invitation invitation) {
		new InvitationDao().updateInvitation(invitation);
	}
	
	@POST
	@Path("/invite/pending")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public List<Invitation> getPendingReceivedInvitation(Invitation invitation) {
		return new InvitationDao().getPendingReceivedInvitation(invitation);
	}
	
	@POST
	@Path("/invite/sent")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public List<Invitation> getPendingSentInvitation(Invitation invitation) {
		return new InvitationDao().getPendingSentInvitation(invitation);
	}
	
    @GET
    @Path("/invite/list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Invitation> getIvitationList() {
        return new InvitationDao().getAllInivitations();
    }

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Student> getStudentList() {
        return new StudentDao().getAllStudents();
    }
    
	@POST
	@Path("/data")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public List<Student> getStudent(Student student) {
    	return new StudentDao().getStudentByFacebookId(student.getStudentFbId());
	}
  
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void login(Student student) throws Exception{
    	List<Student> studentList = new StudentDao().getStudentByFacebookId(student.getStudentFbId());
    	if (studentList.isEmpty() || studentList == null){
    		new StudentDao().createStudent(student);
    	}else{
    		new StudentDao().updateStudent(student);
    	}
    }
    
    @POST
    @Path("/coordinates")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCoordinates(Student student) throws Exception{
    	new StudentDao().updateStudentCoordinates(student);
    }
}
