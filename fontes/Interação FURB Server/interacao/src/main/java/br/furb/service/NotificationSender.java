package br.furb.service;

import java.util.Arrays;
import java.util.List;
import org.jboss.aerogear.unifiedpush.DefaultPushSender;
import org.jboss.aerogear.unifiedpush.PushSender;
import org.jboss.aerogear.unifiedpush.message.UnifiedMessage;
import br.furb.dao.StudentDao;
import br.furb.model.Event;
import br.furb.model.Invitation;
import br.furb.model.Student;

public class NotificationSender {

	public PushSender getPushSender() {
		PushSender defaultPushSender = DefaultPushSender.withRootServerURL("http://10.13.2.138:8080/ag-push")
				.pushApplicationId("2c0623e6-7bb7-4050-ac7f-0b3928a695d9")
				.masterSecret("e0d434ec-ec3c-4be3-977e-3b132f1df5a6").build();
		return defaultPushSender;
	}

	void sendInvitationNotification(Invitation invitation) {
		List<Student> studentList = new StudentDao().getStudentByFacebookId(invitation.getInvitationSender().getStudentFbId());
		String senderName = "";
		if (studentList.isEmpty() || studentList != null){
			senderName = studentList.get(0).getStudentName();
		}
		
		UnifiedMessage unifiedMessage = UnifiedMessage.withCriteria()
	                .aliases(Arrays.asList(invitation.getInvitationRecipient().getStudentFbId().toString()))
	                .message()
	                  .alert(senderName + " quer compartilhar sua localização!")
	                  .sound("default")
	                  .badge("1")
	                .build();
		this.getPushSender().send(unifiedMessage);
	}
	
	void sendEventNotification(Event event) {
		
		UnifiedMessage unifiedMessage = UnifiedMessage.withCriteria()
	                .aliases()
	                .message()
	                  .alert(event.getContent())
	                  .sound("default")
	                  .badge("1")
	                .build();
		this.getPushSender().send(unifiedMessage);
	}
}
