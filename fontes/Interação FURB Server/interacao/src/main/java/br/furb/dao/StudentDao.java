package br.furb.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import br.furb.model.Student;

public class StudentDao {

	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("interacao");
		this.entityManager = emf.createEntityManager();
		return this.entityManager;
	}

	public List<Student> getAllStudents() {
		Query query = this.getEntityManager().createQuery("from Student");
		return query.getResultList();
	}

	public List<Student> getStudentByFacebookId(Long studentFbId) {
		Query query = this.getEntityManager().createQuery("from Student where studentFbId = :studentFbId");
		query.setParameter("studentFbId", studentFbId);
		return query.getResultList();
	}

	public void createStudent(Student student) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(student);
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}
	
	public void mergeStudent(Student student) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.merge(student);
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}

	public void updateStudent(Student student) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			Query query = em.createQuery("update Student set studentName = :studentName,"
					+ " studentFbToken = :studentFbToken, studentStatus = :studentStatus" 
					+ " where studentFbId = :studentFbId");
			query.setParameter("studentName", student.getStudentName());
			query.setParameter("studentFbToken", student.getStudentFbToken());
			query.setParameter("studentFbId", student.getStudentFbId());
			query.setParameter("studentStatus", student.getStudentStatus());
			query.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}

	public void updateStudentCoordinates(Student student) {
		EntityManager em = this.getEntityManager();
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			Query query = em.createQuery("update Student set studentLatitude = :studentLatitude,"
					+ " studentLongitude = :studentLongitude" + " where studentFbId = :studentFbId");
			query.setParameter("studentLatitude", student.getStudentLatitude());
			query.setParameter("studentLongitude", student.getStudentLongitude());
			query.setParameter("studentFbId", student.getStudentFbId());
			query.executeUpdate();
		} catch (Exception e) {
			System.out.println("ERROR:" + e);
			tx.rollback();
		} finally {
			if (tx.isActive()) {
				tx.commit();
			}
		}
	}
}
